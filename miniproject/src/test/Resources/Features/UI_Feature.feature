Feature: This feature is to validate the UI of the Mini Project
Background: This Background is to set the driver instance
	Given set the driver instance
	
	@UserValidation
	Scenario: This scenario is to Validate login through UI
		Given user navigates to the web-page
		When user enters username and password in the web-page
		And clicks the login button
		Then validate username on Books DashBoard
		
	@BookValidation	
	Scenario: This scenario is to extract books title publisher and title from the web-page
		Given user navigates to the web-page
		When user enters username and password in the web-page
		And clicks the login button
		Then ectract the title, publisher and author
		
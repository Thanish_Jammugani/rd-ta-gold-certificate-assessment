package com.miniproject.validations;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.miniproject.filereader.FileInstance;
import com.miniproject.logs.Log;

import io.restassured.response.Response;

import static com.miniproject.constants.Constants.*;
import static org.hamcrest.Matchers.equalTo;


public class Validations {
	
	@Test
	public void statusCodeVaidate(int statusCodeActual, int statusCodeExpected) {
		Assert.assertEquals(statusCodeActual, statusCodeExpected);
		Log.logInfo("Status code of account created is validated");
	}
	
	@Test
	public void responseBodyValidation(Response response) {
		try {
			response.then().body("username", equalTo(FileInstance.getFileData(LOGIN_DETAILS_PATH, "userName")));
			Log.logInfo("Response Body of account created is validated");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void homePageUserNameValidation(String username) {
		try {
			String userName = FileInstance.getFileData(LOGIN_DETAILS_PATH, "userName");
			Assert.assertEquals(userName, username);
			Log.logInfo("User is validated");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

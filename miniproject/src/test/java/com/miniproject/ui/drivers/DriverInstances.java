package com.miniproject.ui.drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.miniproject.exceptions.InvalidDriverException;

public class DriverInstances {
	public static WebDriver getInstance(String requiredDriver) throws InvalidDriverException {
		WebDriver driver;
		switch (requiredDriver.toLowerCase()) {
			case "chrome": ChromeOptions options = new ChromeOptions();
						   options.addArguments("--remote-allow-origins=*");
						   driver = new ChromeDriver(options);
						   break;
			case "edge": driver = new EdgeDriver();
						 break;
			case "firefox": driver = new FirefoxDriver();
							break;
			default: throw new InvalidDriverException("Enter Valid Driver Instance");
		}
		driver.manage().window().maximize();
		return driver;
	}
}

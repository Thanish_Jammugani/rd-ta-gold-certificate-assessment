package com.miniproject.ui.picocontainer;

import org.openqa.selenium.WebDriver;

import com.miniproject.ui.pages.HomePage;
import com.miniproject.ui.pages.LoginPage;

public class PicoContainer {
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
	public LoginPage getLoginPage() {
		return loginPage;
	}

	public HomePage getHomePage() {
		return homePage;
	}

	public void setInstances(WebDriver driver) {
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
	}
}

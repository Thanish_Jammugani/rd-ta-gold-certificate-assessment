package com.miniproject.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.miniproject.filereader.FileInstance;

import static com.miniproject.constants.Constants.*;

import java.io.IOException;

public class LoginPage {
	private WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy (id = "userName")
	private WebElement username;
	@FindBy (id = "password")
	private WebElement password;
	public void sendUserNameAndPassword() {
			try {
				username.sendKeys(get("userName"));
				password.sendKeys(get("password"));
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	@FindBy (id = "login")
	private WebElement loginButton;
	public void clickLoginButton() {
		Actions actions = new Actions(driver);
		actions.moveToElement(loginButton).click().build().perform();
	}
	
	private String get(String str) throws IOException {
		return FileInstance.getFileData(LOGIN_DETAILS_PATH, str);
	}	
}

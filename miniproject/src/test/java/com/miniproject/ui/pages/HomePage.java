package com.miniproject.ui.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage {
	private WebDriver driver;
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (id = "userName-value")
	private WebElement username;
	public String getUserName() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String str = username.getText();
		return str;
	}
	
	public List<String> getAllTitles() {
		return getList("//div[@class='action-buttons']//a");
	}

	
	public List<String> getAllPublishers() {
		return getList("//div[@class='rt-tbody']/div/div/div[3]");
	}

	public List<String> getAllAuthors() {
		return getList("//div[@class='rt-tbody']/div/div/div[4]");
	}
	
	private List<String> getList(String xpath) {
		List<WebElement> eleList =  driver.findElements(By.xpath(xpath));
		List<String> titles = new ArrayList<>();
		for (WebElement webElement : eleList) {
			titles.add(webElement.getText());
		}
		return titles;
	}
	
}

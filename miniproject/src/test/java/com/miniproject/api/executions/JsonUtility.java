package com.miniproject.api.executions;

import static com.miniproject.constants.Constants.*;

import java.io.IOException;

import org.json.simple.JSONObject;

import com.miniproject.filereader.FileInstance;

public class JsonUtility {
	public static String getLoginData() {
		JSONObject json = new JSONObject();
		
		try {
			json.put("userName", FileInstance.getFileData(LOGIN_DETAILS_PATH, "userName"));
			json.put("password", FileInstance.getFileData(LOGIN_DETAILS_PATH, "password"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json.toString();
	}
}

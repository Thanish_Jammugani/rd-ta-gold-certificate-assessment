package com.miniproject.api.pojos;

import java.util.ArrayList;

public class BooksPojo {
	private ArrayList<Books> booksList;
	private String Book;

	public String getBook() {
		return Book;
	}

	public void setBook(String book) {
		Book = book;
	}

	public ArrayList<Books> getBooksList() {
		return booksList;
	}

	public void setBooksList(ArrayList<Books> booksList) {
		this.booksList = booksList;
	}
}

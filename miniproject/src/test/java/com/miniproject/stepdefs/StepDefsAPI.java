package com.miniproject.stepdefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.miniproject.api.executions.JsonUtility;
import com.miniproject.api.pojos.Books;
import com.miniproject.api.pojos.BooksPojo;
import com.miniproject.filereader.FileInstance;
import com.miniproject.logs.Log;
import com.miniproject.validations.Validations;

import static com.miniproject.constants.Constants.*;

public class StepDefsAPI {
	private Validations validate;
	
	public StepDefsAPI() {
		validate = new Validations();
	}
	
	@Given("user enters Base URI")
	public void user_enters_base_uri() {
	    RestAssured.baseURI = BASE_URI;
	    Log.logInfo("User Entered BaseURI");
	}
	
	@Given("user enters username and password")
	public void user_enters_username_and_password() {
		String data = JsonUtility.getLoginData();
		Response response =given()
								.contentType("application/json")
								.body(data)
							.when()
								.post(ACCOUNT_POST_PATH)
							.then()
								.extract().response();
		validate.statusCodeVaidate(response.getStatusCode(), 201);
		
		validate.responseBodyValidation(response);
		Log.logInfo("User Account is created");
	}
	
	@When("capture the details of the books")
	public void capture_the_details_of_the_books() {
		
		Response response = given()
								.contentType("application/json")
								.basePath(GET_DATA)
							.when()
								.get();
		
		response.prettyPrint();
		
		BooksPojo pojo = response.as(BooksPojo.class);
		System.out.println(pojo.getBooksList().get(0).toString());
		
		
	}
	
	@Then("validate the captured details")
	public void validate_the_captured_details() {
		
	}
}

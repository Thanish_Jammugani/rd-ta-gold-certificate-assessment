package com.miniproject.stepdefs;

import org.openqa.selenium.WebDriver;

import com.miniproject.filereader.FileInstance;
import com.miniproject.logs.Log;
import com.miniproject.ui.drivers.DriverInstances;
import com.miniproject.ui.pages.HomePage;
import com.miniproject.ui.pages.LoginPage;
import com.miniproject.ui.picocontainer.PicoContainer;
import com.miniproject.validations.Validations;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static com.miniproject.constants.Constants.*;

import java.util.List;


public class StepDefsUI {
	private Validations validate;
	private PicoContainer container;
	private WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	
	private List<String> titleList;
	private List<String> authorList;
	private List<String> publisherList;
	
	public StepDefsUI() {
		this.container = new PicoContainer();
		validate = new Validations();
	}
	
	@Given("set the driver instance")
	public void set_the_driver_instance() throws Exception {
	    container.setDriver(DriverInstances.getInstance(FileInstance.getFileData(BROWSER_INSTANCE, "browser")));
	    driver = container.getDriver();
	    container.setInstances(driver);
	    Log.logInfo("Driver instance is set for execution");
	}
	
	@Given("user navigates to the web-page")
	public void user_navigates_to_the_web_page() {
		driver.get(LOGIN_URL);
		loginPage = container.getLoginPage();
		Log.logInfo("User Navigated to the Web Page");
	}
	
	@When("user enters username and password in the web-page")
	public void user_enters_username_and_password_in_the_web_page() {
		loginPage.sendUserNameAndPassword();
		Log.logInfo("UserName and Password is entered into the web page");
	}
	
	@When("clicks the login button")
	public void clicks_the_login_button() {
		loginPage.clickLoginButton();
		homePage = container.getHomePage();
		Log.logInfo("User clicks the login button");
	}
	
	@Then("validate username on Books DashBoard")
	public void validate_username_on_books_dash_board() {
		String username = homePage.getUserName();
		validate.homePageUserNameValidation(username);
		Log.logInfo("User Validation is executed");
	}
	
	@Then("ectract the title, publisher and author")
	public void ectract_the_title_publisher_and_author() {
		titleList = homePage.getAllTitles();
		publisherList = homePage.getAllPublishers();
		authorList = homePage.getAllAuthors();
		
	}
}

package com.miniproject.filereader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class FileInstance {
	public static String getFileData(String path, String value) throws IOException {
		FileInputStream fins = new FileInputStream(path);
		Properties prop = new Properties();
		prop.load(fins);
		return prop.getProperty(value);
	}
}

package com.miniproject.constants;

public class Constants {
	public static final String BASE_URI = "https://demoqa.com/";
	
	public static final String ACCOUNT_POST_PATH = "Account/v1/User";
	
	public static final String LOGIN_DETAILS_PATH = "src//test//Resources//API_LoginDetails.properties";
	public static final String BROWSER_INSTANCE = "src//test//Resources//DriverInstance.properties";
	
	public static final String LOGIN_URL ="https://demoqa.com/login";
	
	public static final String GET_DATA = "BookStore/v1/Books"; 
	
}

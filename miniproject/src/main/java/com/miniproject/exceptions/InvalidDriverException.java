package com.miniproject.exceptions;

public class InvalidDriverException extends Exception {
	public InvalidDriverException(String exception) {
		super(exception);
	}
}

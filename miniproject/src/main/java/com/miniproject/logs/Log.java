package com.miniproject.logs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log {
	private static final Logger LOGGER = LogManager.getLogger();
	
	public static void logInfo(String info) {
		LOGGER.info(info);
	}
	
	public static void loginfo(String infoMessage, Object...param) {
		LOGGER.info(String.format(infoMessage, param));
	}
	
	public static void logDebug(String debugMessage) {
		LOGGER.debug(debugMessage);
	}
	
	public static void logDebug(String debugMessage, Object...param) {
		LOGGER.debug(String.format(debugMessage, param));
	}
}

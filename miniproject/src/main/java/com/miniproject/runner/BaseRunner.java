package com.miniproject.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions (
//					plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
					glue = {"com.miniproject.stepdefs"},
					features = {"src/test/resources/features"},
					tags = "(not @UserCreation) and (not @UserValidation) and (not @BookValidation)",
					dryRun = false)

public class BaseRunner extends AbstractTestNGCucumberTests {
    
}
